#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main(int argv, char **argc) {

	if(argv < 3) {
		cout<<"Insufficient parameters."<<endl;
		system("pause");
		return 0;
	}
	string input_file_name(argc[1]);
	string output_file_name(argc[2]);
	
	ifstream input_file;
	input_file.open(input_file_name);
	if(!input_file.is_open()) {
		cout<<"Input file not find."<<endl;
		system("pause");
		return 0;
	}
	ofstream output_file;
	output_file.open(output_file_name);

	bool multiline_comment_mode = false;

	while(!input_file.eof()) {
		string parse_str;
		getline(input_file, parse_str, '\n');
		if(multiline_comment_mode == true) {
			size_t close_comment = parse_str.find("*/");
			if(close_comment != string::npos) {
				parse_str.erase(0, close_comment + 2);
				multiline_comment_mode = false;
			} else continue;
		}
		size_t comment = parse_str.find("//");
		if(comment != string::npos) {
			parse_str.erase(comment, parse_str.length() - comment);
		}
		while(true) {
			size_t open_comment = parse_str.find("/*");
			if(open_comment != string::npos) {
				size_t close_comment = parse_str.find("*/");
				if(close_comment != string::npos) {
					parse_str.erase(open_comment, close_comment - open_comment + 2);
				} else {
					multiline_comment_mode = true;
					parse_str.erase(open_comment, parse_str.length() - open_comment);
					break;
				}
			} else break;
		}
		output_file<<parse_str<<endl;
	}
	system("pause");
	return 0;
}